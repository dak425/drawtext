package drawtext

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/dak425/easing/ffmpeg"
)

const top = "x=(w-tw)/2:y=0"
const bottom = "x=(w-tw)/2:y=(h-th)"
const left = "x=0:y=(h-th)/2"
const right = "x=(w-tw):y=(h-th)/2"
const center = "x=(w-tw)/2:y=(h-th)/2"
const bottomRight = "x=(w-tw):y=(h-th)"

const fadeExpr = "if(lt(t\\,%f)\\,0\\,if(lt(t\\,%f)\\,%s\\,if(lt(t\\,%f)\\,%f\\,if(lt(t\\,%f)\\,%s\\,0))))"
const fadeInProgress = "(t-%f)/%f"
const fadeOutProgress = "(%f-(t-%f))/%f"

type Options struct {
	Text            string  `json:"text"`
	TextColor       string  `json:"textColor"`
	TextFont        string  `json:"textFont"`
	TextPosition    string  `json:"textPosition"`
	TextSize        int     `json:"textSize"`
	TextAlpha       float64 `json:"textAlpha"`
	TextStart       float64 `json:"textStart"`
	TextEnd         float64 `json:"textEnd"`
	Box             bool    `json:"box"`
	BoxColor        string  `json:"boxColor"`
	BoxAlpha        float64 `json:"boxAlpha"`
	ShadowColor     string  `json:"shadowColor"`
	ShadowAlpha     float64 `json:"shadowAlpha"`
	ShadowX         int     `json:"shadowX"`
	ShadowY         int     `json:"shadowY"`
	FadeInDuration  float64 `json:"fadeInDuration"`
	FadeOutDuration float64 `json:"fadeOutDuration"`
	FadeEase        string  `json:"fadeEase"`
}

func (o Options) String() string {
	return buildFilter(o)
}

func NewOptions(text string) Options {
	return Options{
		Text:            text,
		TextColor:       "black",
		TextFont:        "sans",
		TextPosition:    "center",
		TextSize:        16,
		TextAlpha:       1.0,
		TextStart:       0,
		TextEnd:         0,
		Box:             false,
		BoxColor:        "black",
		BoxAlpha:        1.0,
		ShadowColor:     "black",
		ShadowAlpha:     1.0,
		ShadowX:         0,
		ShadowY:         0,
		FadeInDuration:  0,
		FadeOutDuration: 0,
		FadeEase:        "linear",
	}
}

func EnabledArg(tStart float64, tEnd float64) string {
	if tStart > 0 && tEnd > 0 {
		return fmt.Sprintf("enable=between(t\\,%.1f\\,%.1f)", tStart, tEnd)
	}
	if tStart > 0 {
		return fmt.Sprintf("enable=gte(t\\,%.1f)", tStart)
	}
	if tEnd > 0 {
		return fmt.Sprintf("enable=lte(t\\,,%.1f)", tEnd)
	}
	return "enable=1"
}

func FontArg(font string) string {
	if isFile(font) {
		return fmt.Sprintf("fontfile=%s", font)
	}
	return fmt.Sprintf("font=%s", font)
}

func TextArg(text string) string {
	if isFile(text) {
		return fmt.Sprintf("textfile=%s", text)
	}
	return fmt.Sprintf("text=%s", text)
}

func TextColorArg(color string) string {
	return fmt.Sprintf("fontcolor=%s", color)
}

func TextSizeArg(size int) string {
	return fmt.Sprintf("fontsize=%d", size)
}

func TextAlphaArg(alpha float64) string {
	return fmt.Sprintf("alpha=%.1f", alpha)
}

func TextPositionArg(position string) string {
	switch position {
	case "top":
		return top
	case "bottom":
		return bottom
	case "left":
		return left
	case "right":
		return right
	case "center":
		return center
	case "bottomRight":
		return bottomRight
	default:
		return center
	}
}

func BoxArg(enabled bool) string {
	if enabled {
		return "box=1"
	}
	return "box=0"
}

func BoxColorArg(color string, alpha float64) string {
	return fmt.Sprintf("boxcolor=%s@%.1f", color, alpha)
}

func ShadowColorArg(color string, alpha float64) string {
	return fmt.Sprintf("shadowcolor=%s@%.1f", color, alpha)
}

func ShadowOffsetArg(x int, y int) string {
	return fmt.Sprintf("shadowx=%d:shadowy=%d", x, y)
}

func buildFilter(options Options) string {
	var textAlphaArg string

	fontArg := FontArg(options.TextFont)
	textArg := TextArg(options.Text)
	textColorArg := TextColorArg(options.TextColor)
	if options.FadeInDuration > 0 || options.FadeOutDuration > 0 {
		easeStrFunc, err := ffmpeg.FuncFromStr(options.FadeEase)
		if err != nil {
			log.Printf("Invalid easing func name given '%s', falling back to linear", options.FadeEase)
			easeStrFunc, _ = ffmpeg.FuncFromStr("linear")
		}
		textAlphaArg = fadeExpression(
			options.TextAlpha,
			options.FadeInDuration,
			options.FadeOutDuration,
			options.TextStart,
			options.TextEnd-options.TextStart,
			easeStrFunc,
		)
	} else {
		textAlphaArg = TextAlphaArg(options.TextAlpha)
	}
	textSizeArg := TextSizeArg(options.TextSize)
	textPositionArg := TextPositionArg(options.TextPosition)
	boxArg := BoxArg(options.Box)
	boxColorArg := BoxColorArg(options.BoxColor, options.BoxAlpha)
	shadowColorArg := ShadowColorArg(options.ShadowColor, options.ShadowAlpha)
	shadowOffsetArg := ShadowOffsetArg(options.ShadowX, options.ShadowY)
	enabledArg := EnabledArg(options.TextStart, options.TextEnd)
	return fmt.Sprintf(
		"drawtext=%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s",
		fontArg,
		textArg,
		textColorArg,
		textSizeArg,
		textAlphaArg,
		textPositionArg,
		boxArg,
		boxColorArg,
		shadowColorArg,
		shadowOffsetArg,
		enabledArg,
	)
}

func isFile(path string) bool {
	info, err := os.Stat(path)
	return !(os.IsNotExist(err) || info.IsDir())
}

func fadeExpression(alpha float64, fadeInDuration float64, fadeOutDuration float64, start float64, duration float64, easingStrFunc func(string) string) string {
	fadeInExpr := fmt.Sprintf(fadeInProgress, start, fadeInDuration)
	fadeOutExpr := fmt.Sprintf(fadeOutProgress, fadeOutDuration, duration, fadeOutDuration)
	expr := fmt.Sprintf(
		fadeExpr,
		start,
		start+fadeInDuration,
		easingStrFunc(fadeInExpr),
		duration,
		alpha,
		duration+fadeOutDuration,
		easingStrFunc(fadeOutExpr),
	)
	return fmt.Sprintf("alpha=%s", expr)
}
