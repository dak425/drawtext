package main

import (
	"flag"
	"log"
	"os"
	"os/exec"

	"gitlab.com/dak425/drawtext"
)

var ffmpegPath string = "ffmpeg"
var ffplayPath string = "ffplay"

func renderText(input string, output string, options drawtext.Options) {
	args := []string{
		"-i",
		input,
		"-vf",
		options.String(),
		"-y",
		output,
	}
	cmd := exec.Command(ffmpegPath, args...)
	log.Printf("Rendering video with: %v\n", cmd.Args)
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Error when rendering text: %s", err)
	}
	log.Print("Video rendering complete!")
	os.Exit(0)
}

func previewText(input string, options drawtext.Options) {
	args := []string{
		"-i",
		input,
		"-vf",
		options.String(),
	}
	cmd := exec.Command(ffplayPath, args...)
	log.Printf("Previewing with: %v\n", cmd.Args)
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Error when previewing text render: %s", err)
	}
	os.Exit(0)
}

func main() {
	var input, output string
	var text, textFont, textColor, textPosition, fadeEase string
	var textSize int
	var textStart, textEnd, textAlpha, fadeIn, fadeOut float64
	var box bool
	var boxColor string
	var boxAlpha float64
	var shadowColor string
	var shadowAlpha float64
	var shadowX, shadowY int
	var help, render bool

	flag.StringVar(&input, "input", "", "File path to the video that will have the text rendered on it. Required")
	flag.StringVar(&output, "output", "", "File path to write the rendered video to. Required if rendering")
	flag.StringVar(&text, "text", "", "The text or text file to render onto the video. Required")
	flag.StringVar(&textFont, "font", "sans", "The font to use when rendering the text. Default is 'sans'")
	flag.StringVar(&textColor, "color", "black", "The color to use when rendering the text. Default is black")
	flag.StringVar(&textPosition, "position", "center", "The position to place the text on the video. Default is center")
	flag.IntVar(&textSize, "size", 16, "The size to set the text to. Default is 16")
	flag.Float64Var(&textStart, "start", 0, "The time, in seconds, to start showing the text on the video. Default is 0")
	flag.Float64Var(&textEnd, "end", 0, "The time, in seconds, to stop showing the text on the video. Default is 0")
	flag.Float64Var(&textAlpha, "alpha", 1.0, "Transparency of the text. Default is 1")
	flag.BoolVar(&box, "box", false, "Draws a box behind the text. Default is false")
	flag.StringVar(&boxColor, "boxcolor", "black", "The color to use when drawing a box behind the text. Default is black")
	flag.Float64Var(&boxAlpha, "boxalpha", 1.0, "Transparency of the text box. Default is 1")
	flag.StringVar(&shadowColor, "shadowcolor", "black", "Color to use for the text drop shadow. Default is black")
	flag.Float64Var(&shadowAlpha, "shadowalpha", 1.0, "Transparency of the text drop shadow. Default is 1")
	flag.IntVar(&shadowX, "shadowx", 0, "X coordinate offset of the drop shadow. Default is 0")
	flag.IntVar(&shadowY, "shadowy", 0, "Y coordinate offset of the drop shadow. Default is 0")
	flag.Float64Var(&fadeIn, "fadein", 0, "Number of seconds to animate a fade in effect. Default is 0")
	flag.Float64Var(&fadeOut, "fadeout", 0, "Number of seconds to animate a fade out effect. Default is 0")
	flag.StringVar(&fadeEase, "fadeease", "linear", "The type of easing function to apply to the fade in effect. Default is linear")
	flag.BoolVar(&render, "render", false, "Enables writing the video out to a file. Default is false")
	flag.BoolVar(&help, "help", false, "Displays the usage information")

	flag.Parse()

	if help {
		flag.PrintDefaults()
		os.Exit(0)
	}

	if input == "" {
		log.Fatalf("Input path is required")
	}

	options := drawtext.NewOptions(text)
	options.TextFont = textFont
	options.TextColor = textColor
	options.TextPosition = textPosition
	options.TextSize = textSize
	options.TextStart = textStart
	options.TextEnd = textEnd
	options.TextAlpha = textAlpha
	options.Box = box
	options.BoxColor = boxColor
	options.BoxAlpha = boxAlpha
	options.ShadowColor = shadowColor
	options.ShadowAlpha = shadowAlpha
	options.ShadowX = shadowX
	options.ShadowY = shadowY
	options.FadeInDuration = fadeIn
	options.FadeOutDuration = fadeOut
	options.FadeEase = fadeEase

	if render {
		if output == "" {
			log.Fatalf("Output path is required when rendering")
		}
		renderText(input, output, options)
	}
	previewText(input, options)
}
